import {shallow} from '@vue/test-utils'
import Sorteio from '../src/components/Sorteio.vue'

describe('Sorteio.vue',function(){
    
    it('Verifica se numero é menor que 60',function(){

        const wrapper = shallow(Sorteio)

        const button = wrapper.find('input')

        button.trigger('click')

        const colunas = wrapper.findAll('td')
                
        let number = parseInt(colunas.at(0).text())
        expect(true).toBe(number < 60)

        number = parseInt(colunas.at(1).text())
        expect(true).toBe(number < 60)

        number = parseInt(colunas.at(2).text())
        expect(true).toBe(number < 60)

        number = parseInt(colunas.at(3).text())
        expect(true).toBe(number < 60)

        number = parseInt(colunas.at(4).text())
        expect(true).toBe(number < 60)

        number = parseInt(colunas.at(5).text())
        expect(true).toBe(number < 60)
    
    })
})